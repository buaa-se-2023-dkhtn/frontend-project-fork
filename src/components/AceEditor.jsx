import { useEffect } from "react";
import { useScript } from "../utils/useScript";

const AceEditor = () => {
    useEffect(() => {
        const url = "https://cdnjs.cloudflare.com/ajax/libs/ace/1.16.0/ace.js";
        const script = document.createElement('script')
        script.src = url
        script.async = true
        script.onload = () => {
            var editor = window.ace.edit("editor");
            editor.setTheme("ace/theme/monokai");
            editor.session.setMode("ace/mode/golang");
        }
        document.body.appendChild(script)
        
        return () => {
            document.body.removeChild(script)
        }
    }, []);
    return (<div>
        <div id="editor" style={{ position: 'flex', minHeight: 250}}></div>
    </div>);
}

export default AceEditor;